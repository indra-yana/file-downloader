package com.training.filedownloader.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.training.filedownloader.R
import com.training.filedownloader.api.ApiClient
import com.training.filedownloader.api.ApiInterface
import com.training.filedownloader.data.AppDownloaderStatus
import com.training.filedownloader.repositories.AppDownloaderRepository
import com.training.filedownloader.utils.checkPermission
import com.training.filedownloader.utils.enable
import com.training.filedownloader.utils.requestPermission
import com.training.filedownloader.viewmodels.AppDownloaderViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 11.15
 * https://gitlab.com/indra-yana
 ****************************************************/

class MainActivity : AppCompatActivity() {

    private var isDownloading = false
    private var downloadURL = ""    // TODO: Add file url
    private var fileName = downloadURL.substring(downloadURL.lastIndexOf("/")+1)

    private lateinit var viewModel : AppDownloaderViewModel

    companion object {
        private val PERMISSION_NAME = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        private const val PERMISSION_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = AppDownloaderViewModel(AppDownloaderRepository(ApiClient.createApiClient(ApiInterface::class.java)))
        viewModel.downloadStatus.observe(this, Observer {
            isDownloading = it is AppDownloaderStatus.Progress
            btnDownload.enable(it !is AppDownloaderStatus.Progress)

            when (it) {
                is AppDownloaderStatus.Progress -> {
                    tvProgress.text = ("Downloading File: ${it.percent} %")
                    progressDownload.progress = it.percent
                    Log.d("startDownload", "Downloading File: ${it.percent} %")
                }
                is AppDownloaderStatus.Finished -> {
                    progressDownload.progress = 100
                    tvProgress.text = ("Download Finished! File saved to: ${it.file.absolutePath}")
                    Log.d("startDownload", "Download Finished! ${it.file.absolutePath}")
                }
                is AppDownloaderStatus.Failed -> {
                    tvProgress.text = ("Download Failed! ${it.errorMessage}")
                    Log.d("startDownload", "Download Failed! ${it.errorMessage}")
                }
            }
        })

        btnDownload.setOnClickListener {
            if (checkPermission(PERMISSION_NAME[0]) && !isDownloading) {
                lifecycleScope.launch {
                    viewModel.startDownload(this@MainActivity, downloadURL, fileName)
                }
            } else {
                requestPermission(
                    PERMISSION_NAME,
                    PERMISSION_CODE
                )
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!isDownloading) {
                        lifecycleScope.launch {
                            viewModel.startDownload(this@MainActivity, downloadURL, fileName)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Permission Denied, Please allow to proceed!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }


    /** Optional method if you want to direct download in the Activity or Fragment not using ViewModel Class */
    /*
   private fun ResponseBody.writeFile(dir: File?, fileName: String): Flow<DownloaderStatus> {
       return  flow {
           emit(DownloaderStatus.Progress(0))

           var deleteFile = true
           val file = File(dir, fileName)

           try {
               byteStream().use { inputStream ->
                   file.outputStream().use { outputStream ->
                       val totalBytes = contentLength()
                       val data = ByteArray(4096)
                       var progressBytes = 0L

                       while (true) {
                           val bytes = inputStream.read(data)
                           if (bytes == -1) break

                           outputStream.channel
                           outputStream.write(data, 0, bytes)
                           progressBytes += bytes

                           emit(DownloaderStatus.Progress(((progressBytes * 100) / totalBytes).toInt()))
                       }

                       when {
                           progressBytes < totalBytes -> throw  Exception("Missing Bytes!")
                           progressBytes > totalBytes -> throw  Exception("Too many bytes!")
                           else -> deleteFile = false
                       }
                   }

               }

               emit(DownloaderStatus.Finished(file))
           } catch (ex: Exception) {
               emit(DownloaderStatus.Failed(ex.message.toString()))
           } finally {
               // check if download was successful
               if (deleteFile) {
                   file.delete()
                   Log.d("startDownload", "File Deleted From Storage!")
               }
           }

       }.flowOn(Dispatchers.IO).distinctUntilChanged()
   }

   @SuppressLint("SetTextI18n")
   private suspend fun Context.startDownload() {
       val dir = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)

       coroutineScope {
           ApiClient().apply {
               try {
                   download("system/App/LauncherApp/apks/000/000/015/original/centrin.iptvlauncher.1.5.8-debug.apk")
                       .writeFile(dir, "launcher_app.apk").collect {
                           when (it) {
                               is DownloaderStatus.Progress -> {
                                   // TODO: update ui with progress

                                   isDownloading = true
                                   tvProgress.text = "Downloading File: ${it.percent} %"
                                   progressDownload.progress = it.percent
                                   Log.d("startDownload", "Downloading File: ${it.percent} %")
                               }
                               is DownloaderStatus.Finished -> {
                                   // TODO: update ui with file

                                   isDownloading = false
                                   tvProgress.text = "Downloading Finished! File saved to: ${it.file.absolutePath}"
                                   Log.d("startDownload", "Download Finished! ${it.file.absolutePath}")
                               }
                               is DownloaderStatus.Failed -> {
                                   tvProgress.text = "Download Failed! ${it.errorMessage}"
                                   isDownloading = false
                                   Log.d("startDownload", "Download Failed! ${it.errorMessage}")
                               }
                           }
                       }
               } catch (ex: Exception) {
                   Log.d("startDownload", ex.message.toString())
               }

           }
       }
   }

   private fun writeResponseBodyToDisk(body: ResponseBody): Boolean {
       return try {
           // todo change the file location/name according to your needs
           val futureStudioIconFile =
               File(getExternalFilesDir(null).toString() + File.separator + "app.apk")
           var inputStream: InputStream? = null
           var outputStream: OutputStream? = null

           try {
               val fileReader = ByteArray(4096)
               val fileSize = body.contentLength()
               var fileSizeDownloaded: Long = 0

               inputStream = body.byteStream()
               outputStream = FileOutputStream(futureStudioIconFile)

               while (true) {
                   val read: Int = inputStream.read(fileReader)
                   if (read == -1) {
                       break
                   }
                   outputStream.write(fileReader, 0, read)
                   fileSizeDownloaded += read.toLong()
                   Log.d(
                       "writeResponseBodyToDisk",
                       "file download: $fileSizeDownloaded of $fileSize"
                   )
               }
               outputStream.flush()
               true
           } catch (e: IOException) {
               false
           } finally {
               inputStream?.close()
               outputStream?.close()
           }
       } catch (e: IOException) {
           false
       }
   }
    */

}
