package com.training.filedownloader.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 13.08
 * https://gitlab.com/indra-yana
 ****************************************************/
class ApiClient {

    companion object {
        private const val BASE_URL = ""     // TODO: Add file base URL
        private const val API_KEY = "your_secret_api_key"

        @Volatile
        private var retrofit: Retrofit? = null

        @Synchronized
        fun getRetrofit(): Retrofit {
            if (retrofit == null) {
                // Request interceptor
                val requestInterceptor = Interceptor { chain ->
                    val urlBuilder = chain.request()
                        .url
                        .newBuilder()
                        .addQueryParameter(
                            "api_key",
                            API_KEY
                        )
                        .build()

                    val requestHeaders = chain.request()
                        .newBuilder()
                        .url(urlBuilder)
                        .addHeader("Accept", "application/json")
                        .build()

                    return@Interceptor chain.proceed(requestHeaders)
                }

                // Add Logging interceptor
                val loggingInterceptor = HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                }

                // OkHttpClient
                val okHttpClient = OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor)
                    .addInterceptor(requestInterceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build()

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            }

            return retrofit!!
        }

        fun <Api> createApiClient(api: Class<Api>): Api {
            return getRetrofit().create(api)
        }
    }

}