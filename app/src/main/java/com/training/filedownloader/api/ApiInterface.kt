package com.training.filedownloader.api

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 13.10
 * https://gitlab.com/indra-yana
 ****************************************************/

interface ApiInterface {

    @Streaming
    @GET
    suspend fun downloadApp(@Url url: String) : ResponseBody

}