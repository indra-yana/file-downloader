package com.training.filedownloader.data

import java.io.File

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 13.15
 * https://gitlab.com/indra-yana
 ****************************************************/

sealed class AppDownloaderStatus {
    data class Progress(val percent: Int) : AppDownloaderStatus()
    data class Finished(val file: File) : AppDownloaderStatus()
    data class Failed(val errorMessage: String) : AppDownloaderStatus()
}