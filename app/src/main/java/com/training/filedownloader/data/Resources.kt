package com.training.filedownloader.data

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 13.17
 * https://gitlab.com/indra-yana
 ****************************************************/

sealed class Resources<out T> {
    data class Success<out T>(val value: T) : Resources<T>()
    data class Failure(val errorMessage: String?) : Resources<Nothing>()
    data class Download<out T>(val value: T) : Resources<T>()
    data class Loading(val isLoading: Boolean) : Resources<Nothing>()
}