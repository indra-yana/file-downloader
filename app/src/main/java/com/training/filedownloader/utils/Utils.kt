package com.training.filedownloader.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 14.10
 * https://gitlab.com/indra-yana
 ****************************************************/

fun Context.checkPermission(permission: String): Boolean {
    return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, permission)
}

fun Activity.requestPermission(permissionList: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(this,
        permissionList,
        requestCode
    )
}

fun View.visible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.enable(enabled: Boolean) {
    isEnabled = enabled
    alpha = if (enabled) 1f else 0.5f
}
