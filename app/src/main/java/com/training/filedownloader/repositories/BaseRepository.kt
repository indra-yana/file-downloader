package com.training.filedownloader.repositories

import com.training.filedownloader.data.Resources
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 14.02
 * https://gitlab.com/indra-yana
 ****************************************************/

abstract class BaseRepository {

    suspend fun <T> safeApiCall(apiCall: suspend () -> T) : Resources<T> {
        return withContext(Dispatchers.IO) {
            try {
                Resources.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                Resources.Failure(throwable.message.toString())
            }
        }
    }

}