package com.training.filedownloader.repositories

import com.training.filedownloader.api.ApiInterface
import okhttp3.ResponseBody

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 13.25
 * https://gitlab.com/indra-yana
 ****************************************************/

class AppDownloaderRepository(private val api: ApiInterface) : BaseRepository() {

    suspend fun downloadApp(url: String) : ResponseBody {
        return api.downloadApp(url)
    }


}