package com.training.filedownloader.viewmodels

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.training.filedownloader.data.AppDownloaderStatus
import com.training.filedownloader.data.Resources
import com.training.filedownloader.repositories.AppDownloaderRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import okhttp3.ResponseBody
import java.io.File

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On Wednesday, 20/01/2021 14.08
 * https://gitlab.com/indra-yana
 ****************************************************/

class AppDownloaderViewModel(private val repository: AppDownloaderRepository) : ViewModel() {

    private var _downloadStatus : MutableLiveData<AppDownloaderStatus> = MutableLiveData()
    val downloadStatus : LiveData<AppDownloaderStatus> get() = _downloadStatus

    @SuppressLint("SetTextI18n")
    suspend fun startDownload(context: Context, url: String, fileName: String) {
        val fileDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)

        repository.apply {
            try {
                downloadApp(url)
                    .writeFile(fileDir, fileName).collect {
                        when (it) {
                            is Resources.Download<AppDownloaderStatus> -> {
                                when(val downloadStatus = it.value) {
                                    is AppDownloaderStatus.Progress -> {
                                        _downloadStatus.value =
                                            AppDownloaderStatus.Progress(
                                                downloadStatus.percent
                                            )
                                    }
                                    is AppDownloaderStatus.Finished -> {
                                        _downloadStatus.value =
                                            AppDownloaderStatus.Finished(
                                                downloadStatus.file
                                            )
                                    }
                                    is AppDownloaderStatus.Failed -> {
                                        _downloadStatus.value =
                                            AppDownloaderStatus.Failed(
                                                downloadStatus.errorMessage
                                            )
                                    }
                                }
                            }
                        }
                    }
            } catch (ex: Exception) {
                _downloadStatus.value =
                    AppDownloaderStatus.Failed(
                        ex.message.toString()
                    )
                Log.d("startDownload", ex.message.toString())
            }

        }
    }

    private fun ResponseBody.writeFile(fileDir: File?, fileName: String): Flow<Resources<AppDownloaderStatus>> {
        return  flow {

            var downloadProgress = 0
            var deleteFile = true
            val file = File(fileDir, fileName)

            emit(
                Resources.Download(
                    AppDownloaderStatus.Progress(
                        downloadProgress
                    )
                )
            )

            try {
                byteStream().use { inputStream ->
                    file.outputStream().use { outputStream ->
                        val totalBytes = contentLength()
                        val data = ByteArray(4096)
                        var progressBytes = 0L

                        while (true) {
                            val bytes = inputStream.read(data)
                            if (bytes == -1) break

                            outputStream.channel
                            outputStream.write(data, 0, bytes)
                            progressBytes += bytes

                            downloadProgress = ((progressBytes * 100) / totalBytes).toInt()
                            emit(
                                Resources.Download(
                                    AppDownloaderStatus.Progress(
                                        downloadProgress
                                    )
                                )
                            )
                        }

                        when {
                            progressBytes < totalBytes -> throw  Exception("Missing Bytes!")
                            progressBytes > totalBytes -> throw  Exception("Too many bytes!")
                            else -> deleteFile = false
                        }
                    }

                }
                emit(
                    Resources.Download(
                        AppDownloaderStatus.Finished(
                            file
                        )
                    )
                )
            } catch (ex: Exception) {
                emit(
                    Resources.Download(
                        AppDownloaderStatus.Failed(ex.message.toString())
                    )
                )
            } finally {
                // check if download was unsuccessful and delete the uncompleted downloaded file
                if (deleteFile) {
                    file.delete()
                    Log.d("startDownload", "File Deleted From Storage!")
                }
            }

        }.flowOn(Dispatchers.IO).distinctUntilChanged()
    }

}